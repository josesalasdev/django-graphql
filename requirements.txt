Django==3.0.4
pytest-django==3.8.0
flake8==3.7.9
bandit==1.6.2
pytest-cov==2.8.1
radon