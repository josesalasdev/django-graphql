## Example of django Api with graphql


[![pipeline status](https://gitlab.com/developerjoseph/django-graphql/badges/master/pipeline.svg)](https://gitlab.com/developerjoseph/django-graphql/-/commits/master) [![coverage report](https://gitlab.com/developerjoseph/django-graphql/badges/master/coverage.svg)](https://gitlab.com/developerjoseph/django-graphql/-/commits/master)
### Requirements
- Docker and docker-compose

### Installation
- docker-compose up